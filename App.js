import React, { Component } from 'react';
import AppStack from './src/router.js';

export default class App extends Component<{}> {
  render() {
    return (
      <AppStack />
    );
  }
}