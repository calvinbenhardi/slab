import { StackNavigator } from 'react-navigation';

import Login from './modules/login/index.js';
import Profile from './modules/profile/index.js';
import Rank from './modules/rank/index.js';

const AppStack = StackNavigator({
    Login: {
        screen: Login,
    },
    Profile: {
        screen: Profile,
    },
    Rank: {
        screen: Rank,
    },
},
    { headerMode: 'none' },
);

export default AppStack