import React from 'react';
import { ImageBackground, View, StyleSheet, Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Button, Text, Body, Footer, FooterTab } from 'native-base';

import { Col, Row, Grid } from 'react-native-easy-grid';
import { navigate } from 'react-navigation';
import Icon from 'react-native-vector-icons/EvilIcons';

import styles from './style';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'RAON',
            points: 9999,
            scores: 9999,
            rank: 1
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                < ImageBackground source={require('../../../assets/statics/wooden-color-background.jpg')}
                    style={styles.backgroundImage}>
                    <View style={styles.overlay} />
                    <Text style={[styles.font, styles.profileText]}>Profile</Text>
                    <View style={styles.pictureWrapper}>
                        <Image
                            large
                            source={{ uri: 'https://onepiecetower.tokyo/wp/wp-content/uploads/2017/06/anniversary_summer_01.jpg' }}
                            style={styles.picture}
                        />
                    </View>
                    <Card style={styles.cardName}>
                        <CardItem>
                            <View style={styles.itemPosition}>
                                <Text style={[styles.font, styles.nameText]}>{this.state.name}</Text>
                            </View>
                        </CardItem>
                    </Card>
                    <Card style={styles.cardItemContainer}>
                        <Grid>
                            <Col>
                                <CardItem style={styles.cardItem}>
                                    <Col>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontScore]}>
                                                    {this.state.scores}
                                                </Text>
                                            </View>
                                        </Row>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontItem]}>
                                                    Scores
                                                </Text>
                                            </View>
                                        </Row>
                                    </Col>
                                </CardItem>
                            </Col>
                            <Col>
                                <CardItem style={styles.cardItem}>
                                    <Col>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontScore]}>
                                                    {this.state.points}
                                                </Text>
                                            </View>
                                        </Row>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontItem]}>
                                                    Points
                                                </Text>
                                            </View>
                                        </Row>
                                    </Col>
                                </CardItem>
                            </Col>
                            <Col>
                                <CardItem style={styles.cardItem}>
                                    <Col>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontScore]}>
                                                    #{this.state.rank}
                                                </Text>
                                            </View>
                                        </Row>
                                        <Row>
                                            <View style={styles.itemPosition}>
                                                <Text style={[styles.font,
                                                styles.fontItem]}>
                                                    Rank
                                                </Text>
                                            </View>
                                        </Row>
                                    </Col>
                                </CardItem>
                            </Col>
                        </Grid>
                    </Card>
                </ImageBackground>
                <Footer>
                    <FooterTab>
                        <Button style={styles.rankButton}
                            onPress={() => navigate('Rank', {})}
                        >
                            <Text style={[styles.font, styles.rankFont]}>View All Rank</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}