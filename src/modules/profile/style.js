import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    pictureWrapper: {
        alignSelf: 'center',
    },
    picture: {
        marginTop: 30,
        width: 260,
        height: 320,
    },
    font: {
        fontFamily: 'Ubuntu-Medium',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    profileText: {
        marginTop: 30,
        fontSize: 20,
        color: 'white'
    },
    nameText: {
        fontSize: 20,
        fontFamily: 'Ubuntu-Bold',
    },
    cardName: {
        flex: 0,
        marginLeft: 45,
        marginTop: -5,
        marginRight: 45,
        height: 60,
        justifyContent: 'center'
    },
    fontScore: {
        fontSize: 25,
        color: 'white'
    },
    fontItem: {
        fontSize: 15,
        color: 'white'
    },
    itemPosition: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    cardItem: {
        height: 80,
        backgroundColor: '#FE7B36',
        marginRight: -1
    },
    cardItemContainer: {
        flex: 0,
        marginLeft: 35,
        marginTop: -5,
        marginRight: 35
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: '#000000',
        opacity: 0.5
    },
    rankButton: {
        marginLeft: -5,
        marginRight: -5,
        backgroundColor: '#FE7B36'
    },
    rankFont: {
        fontSize: 15,
        color: 'white',
    }
});