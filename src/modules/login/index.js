import React from 'react';
import { Image, View, StyleSheet } from 'react-native';
import { Container, Content, Button, Text, Form, Item, Label, Input } from 'native-base';

import { navigate } from 'react-navigation';

import styles from './style';

export default class Login extends React.Component{
    constructor(props){
        super(props)
        this.state= {
            data:[]
        }
    }

    render(){
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <View style={styles.logoWrapper}>
                    <Image style={styles.logo} source={require('../../../assets/statics/stocklab-logo.png')} />
                </View>
                <Content>
                <Form style={styles.loginForm}>
                    <Item floatingLabel>
                        <Label>Username</Label>
                        <Input />
                    </Item>
                    <Item floatingLabel>
                        <Label>Password</Label>
                        <Input />
                    </Item>
                </Form>
                <Button full rounded style={styles.loginButton}
                    onPress={() => navigate('Profile', {})}
                >
                    <Text style={styles.loginText}>Login</Text>
                </Button>
                </Content>
            </Container>
        );
    }
}