import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    logoWrapper: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        marginTop: 10,
    },
    loginButton: {
        backgroundColor: '#FF6C00',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 30,
    },
    loginForm: {
        marginLeft: 10,
        marginRight: 30,
    },
    loginText: {
        color: 'white',
    }
});